package core

import com.beust.klaxon.JsonArray
import com.beust.klaxon.JsonObject
import com.beust.klaxon.Klaxon
import com.beust.klaxon.Parser
import java.io.File
import java.util.*
import kotlin.collections.HashMap

open class GameEntity(
    override val coordinates: Coordinates,
    val id: UUID = UUID.randomUUID(),
    val movable: Boolean = true,
    val stackable: Boolean = false
) : PlanarObject {

}

class Game(
    val tickrate: Double
) {
    val entities: MutableMap<UUID, GameEntity> = mutableMapOf()
//    val units: MutableList<Unit> = mutableListOf()

    companion object {
        val unitClasses: MutableMap<String, UnitClass> = mutableMapOf()
    }

    init {
        this.loadDescriptions(listOf("src/main/kotlin/units.json"))
    }


    fun loadSection(filenames: Iterable<String>, sectionName: String): List<JsonObject> {
        val parser: Parser = Parser.default()

        return filenames.mapNotNull {
            (parser.parse(it) as JsonObject).array<JsonObject>(sectionName)
        }.flatten()
    }

    fun loadDescriptions(filenames: Iterable<String>) {
        loadSection(filenames, "classes").forEach {
            val res = Klaxon().parseFromJsonObject<UnitClass>(it)
            if (res != null) Game.unitClasses[res.id] = res
        }
    }

    //TODO names
    fun loadEntities(filenames: Iterable<String>) {
        loadSection(filenames, "units").forEach { unit ->
            val classId = unit.string("class") ?: "null"
            if (Game.unitClasses.containsKey(classId)) {
                val uc = Game.unitClasses[classId]!!
                val u = Unit(uc);
                println(u)
            } else {
            }
        }
    }
}

data class UnitClass(
    val id: String,
    val hp: Int,
    val speed: Double
) {
}

open class Unit(
    val baseHp: Int,
    val baseSpeed: Double,
    var hp: Int = baseHp,
    var speed: Double = baseSpeed,
    id: UUID = UUID.randomUUID(),
    coordinates: Coordinates = Coordinates(Double.NaN, Double.NaN)
) : GameEntity(coordinates, id) {
    constructor(unitClass: UnitClass) : this(unitClass.hp, unitClass.speed)
}


