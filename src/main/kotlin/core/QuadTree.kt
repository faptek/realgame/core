package core

import kotlin.IllegalArgumentException
import kotlin.math.abs
import kotlin.math.pow
import kotlin.math.sqrt

data class Coordinates(var x: Double, var y: Double) {

    val length: Double
        get() = sqrt(this.x.pow(2) + this.y.pow(2))

    infix fun plus(other: Coordinates): Coordinates {
        return Coordinates(this.x + other.x, this.y + other.y)
    }

    infix fun plus(other: Double): Coordinates {
        return Coordinates(this.x + other, this.y + other)
    }

    infix fun minus(other: Coordinates): Coordinates {
        return Coordinates(this.x - other.x, this.y - other.y)
    }

}

interface PlanarObject {
    val coordinates: Coordinates
}

interface QuadTreeContainer<T : PlanarObject> {
    val isQuadrant: Boolean
    fun insert(obj: T): Boolean
    fun queryRange(range: AABB): List<T>
}

data class MinResQuadrant<T : PlanarObject>(
    val points: MutableList<T> = mutableListOf()
) : QuadTreeContainer<T> {
    override val isQuadrant: Boolean = true
    override fun insert(obj: T): Boolean {
        points.add(obj)
        return true
    }

    override fun queryRange(range: AABB): List<T> {
        return this.points.filter {
            range.contains(it.coordinates)
        }
    }
}

data class QuadTreePoint<T : PlanarObject>(
    val value: T
) : QuadTreeContainer<T> {
    override val isQuadrant = false
    // TODO maybe return false?
    override fun insert(obj: T): Boolean {
        throw Error("Trying to insert into point")
    }

    override fun queryRange(range: AABB): List<T> {
        if (range.contains(this.value.coordinates)) {
            return listOf(this.value)
        }
        return listOf()
    }
}

data class AABB(
    val center: Coordinates,
    val halfSide: Double
) {

    init {
        if (!this.halfSide.isFinite()) {
            throw IllegalArgumentException("AABB half-side not finite")
        }
        if (this.halfSide <= 0) {
            throw IllegalArgumentException("AABB half-side non-positive")
        }
        if (!this.center.x.isFinite() || !this.center.y.isFinite()) {
            throw IllegalArgumentException("Not all AABB center coordinates are finite (${this.center})")
        }
        if (this.center.length >= Double.MAX_VALUE / 10) {
            throw IllegalArgumentException("AABB center is in the Far Lands")
        }
        if (this.halfSide > Double.MAX_VALUE / 10) {
            throw IllegalArgumentException("AABB half-side is in the Far Lands")
        }
    }

    fun contains(point: Coordinates, eps: Double = 1e-15): Boolean {
        return abs(this.center.x - point.x) - eps <= this.halfSide &&
                abs(this.center.y - point.y) - eps <= this.halfSide
    }

    infix fun intersectsWith(other: AABB): Boolean {
        return abs(this.center.x - other.center.x) < (this.halfSide + other.halfSide) ||
                abs(this.center.y - other.center.y) < (this.halfSide + other.halfSide)
    }

    fun getQuadrant(point: Coordinates): Int {
        return when {
            point.x >= this.center.x && point.y >= this.center.y -> 0
            point.x < this.center.x && point.y > this.center.y -> 1
            point.x <= this.center.x && point.y <= this.center.y -> 2
            point.x > this.center.x && point.y < this.center.y -> 3
            // TODO raise err
            else -> throw Error("Unreachable code hit: object to insert is in the quadrant, but not in any of its sub-quadrants: $this, ${point}") // Wut?
        }
    }

    fun getQuadrantCenter(quadrantNum: Int): Coordinates {
        return when (quadrantNum) {
            0 -> Coordinates(this.center.x + this.halfSide / 2, this.center.y + this.halfSide / 2)
            1 -> Coordinates(this.center.x - this.halfSide / 2, this.center.y + this.halfSide / 2)
            2 -> Coordinates(this.center.x - this.halfSide / 2, this.center.y - this.halfSide / 2)
            3 -> Coordinates(this.center.x + this.halfSide / 2, this.center.y - this.halfSide / 2)
            // TODO raise err
            else -> throw Error("Unreachable code hit: wrong sub-quadrant number: $quadrantNum")
        }
    }
}

data class Quadrant<T : PlanarObject>(
    val center: Coordinates,
    val halfSide: Double,
    val boundary: AABB = AABB(center, halfSide),
    // + - + - +
    // | 1 | 0 |
    // + - + - +
    // | 2 | 3 +
    // + - + - +
    val points: Array<QuadTreeContainer<T>?> = Array(4) { null },
    val eps: Double = 1e-15,
    val resolution: Double = 1.0
) : QuadTreeContainer<T> {

    init {
        if (this.resolution < 1e-15 || this.resolution > Double.MAX_VALUE / 10) {
            throw IllegalArgumentException("Half-side is in the Far Lands")
        }
    }

    override val isQuadrant = true

    override fun insert(obj: T): Boolean {
        if (!this.boundary.contains(obj.coordinates)) throw IllegalArgumentException("Trying to insert object at ${obj.coordinates} but it's out of bounds for this quadrant\n$this")

        val quadrantNum = this.boundary.getQuadrant(obj.coordinates)

        val pt = points[quadrantNum]

        when (pt) {
            is Quadrant<T> -> return points[quadrantNum]!!.insert(obj)
            is MinResQuadrant<T> -> return points[quadrantNum]!!.insert(obj)
            is QuadTreePoint<T> -> {
                val center = this.boundary.getQuadrantCenter(quadrantNum)
                if (this.boundary.halfSide / 2 <= this.resolution) {
                    points[quadrantNum] = MinResQuadrant()
                } else {
                    points[quadrantNum] = Quadrant(center, this.boundary.halfSide / 2)
                }
                return points[quadrantNum]!!.insert(obj) && points[quadrantNum]!!.insert(pt.value)
            }
            null -> {
                points[quadrantNum] = QuadTreePoint(obj)
                return true
            }
        }
        return false
    }

    /**
     * Checks if given [Coordinates] are in the boundary with given precision
     *
     * @param point [Coordinates] to check
     * @return *true* if the given [Coordinates] are in the boundary + precision, *false* otherwise
     * @author QuentinI
     */
    fun contains(point: Coordinates): Boolean {
        return abs(this.center.x - point.x) - this.eps <= this.halfSide &&
                abs(this.center.y - point.y) - this.eps <= this.halfSide
    }

    override fun queryRange(range: AABB): List<T> {
        if (!(this.boundary intersectsWith range)) return listOf()
        return this.points.mapNotNull {
            it?.queryRange(range)
        }.flatten()
    }

    override fun toString(): String {
        var s = "Quadrant(center = ${this.center}, halfside = ${this.halfSide}, resolution = ${this.resolution})"
        this.points.forEach {
            s += "\n\t- ${it.toString().replace("\n", "\n\t")}"
        }
        return s
    }
}