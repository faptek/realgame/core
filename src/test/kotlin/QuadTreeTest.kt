import core.AABB
import core.Coordinates
import core.GameEntity
import core.Quadrant
import io.kotlintest.*
import io.kotlintest.properties.Gen
import io.kotlintest.properties.assertAll
import io.kotlintest.specs.StringSpec
import kotlin.math.abs
import kotlin.math.min
import kotlin.math.sign
import kotlin.random.Random

class IllegalDoubleGenerator : Gen<Double> {

    override fun always(): Iterable<Double> =
        listOf(Double.NaN, Double.MAX_VALUE, -Double.MAX_VALUE, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY)

    override fun random(): Sequence<Double> = generateSequence {
        val x = Gen.double().random().first()
        Double.MAX_VALUE / 9 * sign(x) + x
    }
}

class LegalDoubleGenerator : Gen<Double> {
    override fun always(): Iterable<Double> = listOf(1.0)

    override fun random(): Sequence<Double> = generateSequence {
        val x = Gen.double().random().first()
        if (abs(x) > Double.MAX_VALUE / 10 || abs(x) < Double.MIN_VALUE * 10) 1.0 else x
    }

}

/* Fixme Names */

class QuadrantTest : StringSpec() {
    init {
        "Quadrant should not accept too high or non-finite parameters" {
            assertAll(IllegalDoubleGenerator(), Gen.double(), Gen.double()) { illegal: Double, x: Double, y: Double ->
                shouldThrow<java.lang.IllegalArgumentException> {
                    Quadrant<GameEntity>(Coordinates(illegal, x), y)
                }
                shouldThrow<java.lang.IllegalArgumentException> {
                    Quadrant<GameEntity>(Coordinates(x, illegal), y)
                }
                shouldThrow<java.lang.IllegalArgumentException> {
                    Quadrant<GameEntity>(Coordinates(x, y), illegal)
                }
            }
        }

        "Quadrant.contains should return whether the given point is within the quadrant's boundary" {
            assertAll(
                LegalDoubleGenerator(),
                LegalDoubleGenerator(),
                LegalDoubleGenerator(),
                LegalDoubleGenerator(),
                LegalDoubleGenerator()
            ) { x: Double, y: Double, hSide: Double, xPoint: Double, yPoint: Double ->

                val center = Coordinates(x, y)
                val quadrant = Quadrant<GameEntity>(center, if (hSide <= 0.0) 1.0 else hSide)
                val point = Coordinates(xPoint, yPoint)

                quadrant.contains(center) shouldBe true
                quadrant.contains(center plus abs(hSide)) shouldBe true
                quadrant.contains(center plus abs(hSide) plus 1.0) shouldBe false
                quadrant.contains(point) shouldBe (point.x <= quadrant.center.x + quadrant.halfSide && point.x >= quadrant.center.x - quadrant.halfSide &&
                        point.y <= quadrant.center.y + quadrant.halfSide && point.y >= quadrant.center.y - quadrant.halfSide)
            }
        }
        "Insertion different objects in one point should work" {
            val q = Quadrant<GameEntity>(Coordinates(0.0, 0.0), 10.0)
//            println(q.insert(GameEntity(Coordinates(9.0, 9.0))))
//            println(q.insert(GameEntity(Coordinates(9.0 - Double.MIN_VALUE, 9.0))))
        }

        "Quadrant center insertion" {
            val center = Coordinates(1e-11, 1e-11)
            val quadrant = Quadrant<GameEntity>(center, 1e-15)
            for (i in 1..100) {
                quadrant.insert(GameEntity(center))
            }
        }


        "Quadrant border insertion" {
            val center = Coordinates(1e-11, 1e-11)
            val quadrant = Quadrant<GameEntity>(center, 1e-1)
            for (i in 1..100) {
                quadrant.insert(GameEntity(center plus quadrant.halfSide))
                quadrant.insert(GameEntity(center plus -quadrant.halfSide))
                quadrant.insert(GameEntity(Coordinates(center.x + quadrant.halfSide, center.y - quadrant.halfSide)))
                quadrant.insert(GameEntity(Coordinates(center.x - quadrant.halfSide, center.y + quadrant.halfSide)))

            }
        }
        "Quadrant random inner point insertion" {
            val center = Coordinates(1e-11, 1e-11)
            val halfSide = 10.0
            val quadrant = Quadrant<GameEntity>(center, halfSide)
            for (i in 0..1000) {
                val minVal = -halfSide
                val x = minVal + (halfSide - minVal) * Random.nextDouble()
                val y = minVal + (halfSide - minVal) * Random.nextDouble()
                val point = center plus Coordinates(x, y)
                quadrant.insert(GameEntity(point))
            }

        }

        "Quadrant random outer point insertion" {
            try {
                val center = Coordinates(1e-11, 1e-11)
                val halfSide = 3.0
                val quadrant = Quadrant<GameEntity>(center, halfSide)
                val minVal = halfSide
                val maxSize = halfSide + 1e-11
                val x = minVal + (maxSize - minVal) * Random.nextDouble()
                val y = minVal + (maxSize - minVal) * Random.nextDouble()
                val point = center plus Coordinates(x, y)
                quadrant.insert(GameEntity(point))
                println(quadrant)
            } catch (e: IllegalArgumentException) {

            }
        }

        "quadrant.queryRange(range).size shouldBe 42" {
            val center = Coordinates(1e-11, 1e-11)
            val halfSide = 3.0
            val quadrant = Quadrant<GameEntity>(center, halfSide)
            for (i in 0..1000) {
                val minVal = -halfSide
                val x = minVal + (halfSide - minVal) * Random.nextDouble()
                val y = minVal + (halfSide - minVal) * Random.nextDouble()
                val point = center plus Coordinates(x, y)
                quadrant.insert(GameEntity(point))
            }
            val range = AABB(center, halfSide)
            quadrant.queryRange(range).size shouldBe 1001
        }

        "Query range inner quadrant" {
            val center = Coordinates(0.0, 0.0)
            val halfSide = 3.0
            val quadrant = Quadrant<GameEntity>(center, halfSide)
            val minVal = -halfSide
            val maxSize = halfSide
            val x = minVal + (maxSize - minVal) * Random.nextDouble()
            val y = minVal + (maxSize - minVal) * Random.nextDouble()
            val newHalfSide = min(halfSide - abs(x), halfSide - abs(y))
            val range = AABB(Coordinates(x, y), newHalfSide)
            var counter: Int = 0
            for (i in 0..1000) {
                var point = Coordinates(x, y)
                val x = minVal + (maxSize - minVal) * Random.nextDouble()
                val y = minVal + (maxSize - minVal) * Random.nextDouble()
                quadrant.insert(GameEntity(point))
                if (range.contains(point)) counter++
            }
            quadrant.queryRange(range).size shouldBe counter
        }


        "Query range half-inner quadrant" {
            val center = Coordinates(0.0, 0.0)
            val halfSide = 3.0
            val quadrant = Quadrant<GameEntity>(center, halfSide)
            val minVal = -halfSide
            val maxSize = halfSide
            val x = halfSide
            val y = halfSide
            val newHalfSide = halfSide
            val range = AABB(Coordinates(halfSide, halfSide), newHalfSide)
            var counter: Int = 0
            for (i in 0..1000) {
                val x = minVal + (maxSize - minVal) * Random.nextDouble()
                val y = minVal + (maxSize - minVal) * Random.nextDouble()
                var point = Coordinates(x, y)
                quadrant.insert(GameEntity(point))
                if (range.contains(point)) {
                    counter++
                }
            }
            quadrant.queryRange(range).size shouldBe counter
        }
    }
}

